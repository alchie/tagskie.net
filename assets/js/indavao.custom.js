(function($) {
    $('document').ready(function() {
        // accordion
        $('#my_sidebar_nav').collapse();
        // Add Shadow on Top Nav
        $(window).scroll( function() {                        
            if( $("body").scrollTop() > 1 ) {
                $('#topnav').addClass('shadow');                
            } else {
                 $('#topnav').removeClass('shadow');   
            }
            if( $("body").scrollTop() > 50 ) {
                $('#social-likes').css({'position' : 'fixed', 'margin-top' : '0' });                
            } else {
                $('#social-likes').css({'position' : 'absolute', 'margin-top' : '50px' });     
            }
        });
        
        $('.unassigned').on('change', function() {
            $('.unassigned option').each(function() {
                    $('.unassigned option[value="'+$(this).val()+'"]').prop('disabled', false);
            });
            $('.unassigned option').each(function() {
                if( $(this).val() != "" && $(this).prop('selected') ) {
                    $('.unassigned option[value="'+$(this).val()+'"]').prop('disabled', true);
                } 
               
            });
            $('.unassigned option:selected').each(function() {
                if( $(this).val() != '' ) {
                    $(this).prop('disabled', false);
                }
            });
        });

        /* frontpage map
            var wrap = $(document).height();
            console.log( wrap - ( parseInt( $('#wrap').css('padding-bottom') ) * 2 ) );
            $('#map-canvas').height(wrap - ( parseInt( $('#wrap').css('padding-bottom') ) * 2 )  );
            $('#frontpage').css('margin-top', -Math.abs(wrap) );
         */   
        var pageLikeLock = function() {
            
        }
        
        $('.selectpicker').selectpicker({
            dropupAuto : false
        });
    });
})(jQuery);
