<?php $this->load->view('overall_header'); ?>
<div class="container homepage" id="frontpage">
        <div class="main">
   
      <div class="row">

<?php 
$n = 0;
if( $videos ) foreach($videos as $video) : 

?>
  <div class="col-sm-4 col-md-3">
    <div class="thumbnail">
      <a href="<?php echo site_url( "watch/" .  $video->video_slug); ?>">
      <?php $image = $video->video_image; 
      if( $video->video_image == '' ) {
		  switch($video->video_source) {
			  case 'youtube':
				$image = '//i.ytimg.com/vi/'.youtube_id( $video->video_url ).'/0.jpg';
			  break;
			  default:
				$image = 'http://www.balaniinfotech.com/wp-content/themes/balani/images/noimage.jpg';
			  break;
		  }
	  }  
      ?>
      <img src="<?php echo $image; ?>" alt="<?php echo $video->video_title; ?>">
      </a>
      <div class="caption text-center">
        <a href="<?php echo site_url( "watch/" . $video->video_slug); ?>"><h3><?php echo $video->video_title; ?></h3></a>
      </div>
    </div>
  </div>
<?php
$n++;
if( $n == 4 ) {
	echo "</div><div class='row'>";
	$n = 0;
}
endforeach; ?>
        
        </div>
        
      </div>
</div> <!-- /container -->
<?php $this->load->view('overall_footer'); ?>
