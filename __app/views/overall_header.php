<!DOCTYPE html>
<!--[if lt IE 7]>      <html class="no-js lt-ie9 lt-ie8 lt-ie7" xmlns="http://www.w3.org/1999/xhtml" xmlns:fb="http://ogp.me/ns/fb#"> <![endif]-->
<!--[if IE 7]>         <html class="no-js lt-ie9 lt-ie8" xmlns="http://www.w3.org/1999/xhtml" xmlns:fb="http://ogp.me/ns/fb#"> <![endif]-->
<!--[if IE 8]>         <html class="no-js lt-ie9" xmlns="http://www.w3.org/1999/xhtml" xmlns:fb="http://ogp.me/ns/fb#"> <![endif]-->
<!--[if gt IE 8]><!--> <html class="no-js" xmlns="http://www.w3.org/1999/xhtml" xmlns:fb="http://ogp.me/ns/fb#"> <!--<![endif]-->
  <head prefix="og: http://ogp.me/ns# fb: http://ogp.me/ns/fb# video: http://ogp.me/ns/video#">
    <title><?php echo $page_title; ?></title>	
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <?php echo $opengraph; ?>  
    <meta property="fb:admins" content="1092830566"/>
    <!-- Bootstrap core CSS -->
    <link href="<?php echo base_url(); ?>assets/js/plugins/bootstrap-select/bootstrap-select.min.css" rel="stylesheet">    
    <link href="<?php echo base_url(); ?>assets/css/bootstrap.min.css" rel="stylesheet">
    <!--<link href="//netdna.bootstrapcdn.com/bootstrap/3.0.3/css/bootstrap.min.css" rel="stylesheet">-->
    <!--<link href="//netdna.bootstrapcdn.com/bootswatch/3.0.3/yeti/bootstrap.min.css" rel="stylesheet">-->

    <!-- Custom styles for this template -->
    <link href="<?php echo base_url(); ?>assets/css/styles.css" rel="stylesheet">

    <!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
      <script src="https://oss.maxcdn.com/libs/respond.js/1.3.0/respond.min.js"></script>
    <![endif]-->
    <script src="http://jwpsrv.com/library/73yjPrO5EeOQuCIACrqE1A.js"></script>

<?php echo $header_css . $header_js . $header; ?>
</head>

  <body>

  <!-- Fixed navbar -->
  <div id="header">
    <div id="topnav" class="navbar navbar-inverse navbar-fixed-top" role="navigation">
      <div class="container">
        <div class="navbar-header">
          <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
            <span class="sr-only">Toggle navigation</span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
          </button>
          <a class="navbar-brand" href="<?php echo ( $isLoggedIn ) ? site_url('my/posts') : base_url(); ?>">Tagskie.Net</a>
        </div>
        <div class="navbar-collapse collapse">
          <ul class="nav navbar-nav">
			  <li><a href="<?php echo site_url('videos'); ?>">Videos</a></li>
              <li><a href="<?php echo site_url('movies'); ?>">Movies</a></li>
              <li><a href="<?php echo site_url('tv-series'); ?>">TV Series</a></li>
            </ul>
          <ul class="nav navbar-nav pull-right">
			  <?php if( $isLoggedIn ) { ?>
			  <li><a href="<?php echo site_url('my/logout'); ?>">Logout</a></li>
			  <?php } else { ?>
              <li><a href="<?php echo site_url('my/posts'); ?>">My Posts</a></li>
              <?php } ?>
            </ul>
        </div><!--/.nav-collapse -->
      </div>
    </div>
  </div>
  <div id="wrap">   
