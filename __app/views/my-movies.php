<?php $this->load->view('overall_header'); ?>
<div class="container homepage" id="frontpage">
        <div class="main">
   
      <div class="row">

<div class="col-md-12">

<ul class="nav nav-tabs nav-justified">
  <li><a href="<?php echo site_url('my/posts'); ?>">Videos</a></li>
  <li class="active"><a href="<?php echo site_url('my/movies'); ?>" style="background-color: #d9edf7; border-color: #bce8f1;">Movies</a></li>
  <li><a href="<?php echo site_url('my/tv_series'); ?>">TV Series</a></li>
</ul>

<div class="well ">

<div class="panel panel-info">
  <div class="panel-heading">
	  <p><a href="<?php echo site_url('my/movies/add'); ?>" class="btn btn-primary btn-sm pull-right">Add Movie</a></p>
    <h3 class="panel-title">My Movies</h3>
  </div>
  <div class="panel-body">
    
    
    <table class="table table-striped">
        <thead>
          <tr>
            <th>#</th>
            <th>First Name</th>
            <th>Last Name</th>
            <th>Username</th>
          </tr>
        </thead>
        <tbody>
          <tr>
            <td>1</td>
            <td>Mark</td>
            <td>Otto</td>
            <td>@mdo</td>
          </tr>
          <tr>
            <td>2</td>
            <td>Jacob</td>
            <td>Thornton</td>
            <td>@fat</td>
          </tr>
          <tr>
            <td>3</td>
            <td>Larry</td>
            <td>the Bird</td>
            <td>@twitter</td>
          </tr>
        </tbody>
      </table>
    
    
  </div>
</div>



</div>

</div>
        
        </div>
        
      </div>
</div> <!-- /container -->
<?php $this->load->view('overall_footer'); ?>
