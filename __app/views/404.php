<?php $this->load->view('overall_header'); ?>
<div class="container homepage" id="frontpage">
        <div class="main">
   
   <div class="well text-center">
		<h1>Page not found!</h1>
   </div>
      <div class="row">

<?php 
$n = 0;
if( $videos ) foreach($videos as $video) : 

?>
  <div class="col-sm-4 col-md-3">
    <div class="thumbnail">
      <a href="<?php echo site_url( "watch/" .  $video->video_slug); ?>">
      <img src="<?php echo $video->video_image; ?>" alt="<?php echo $video->video_title; ?>">
      </a>
      <div class="caption text-center">
        <a href="<?php echo site_url( "watch/" . $video->video_slug); ?>"><h3><?php echo $video->video_title; ?></h3></a>
      </div>
    </div>
  </div>
<?php
$n++;
if( $n == 4 ) {
	echo "</div><div class='row'>";
}
endforeach; ?>
        
        </div>
        
      </div>
</div> <!-- /container -->
<?php $this->load->view('overall_footer'); ?>
