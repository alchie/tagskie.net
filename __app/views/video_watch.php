<?php $this->load->view('overall_header'); ?>
<?php $hashtags = array('Tagskie.Net'); ?>
<div class="container homepage" id="frontpage">
        <div class="main">
     
   </div>
   
      <div class="row">

<div class="col-md-8">
	
	<div id="social-likes">
		<center>
    <div class="social">
		<div class="fb-like" data-href="<?php echo current_url(); ?>" data-layout="box_count" data-action="like" data-show-faces="true" data-share="false"></div>
	</div>
	
	<div class="social">
		<a href="https://twitter.com/share" class="twitter-share-button" data-url="<?php echo current_url(); ?>" data-via="chesteralan" data-lang="en" data-related="Tagskie.Net" data-count="vertical">Tweet</a>
<script>!function(d,s,id){var js,fjs=d.getElementsByTagName(s)[0];if(!d.getElementById(id)){js=d.createElement(s);js.id=id;js.src="https://platform.twitter.com/widgets.js";fjs.parentNode.insertBefore(js,fjs);}}(document,"script","twitter-wjs");</script>
	</div>
	
	<div class="social">
<div class="g-plusone" data-size="tall" data-count="true" data-annotation="bubble" data-width="300" data-href="<?php echo current_url(); ?>"></div>
	</div>
    
    <div class="social">
<script src="//platform.linkedin.com/in.js" type="text/javascript">lang: en_US</script>
<script type="IN/Share" data-url="<?php echo current_url(); ?>" data-counter="top"></script>
		</div>
		</center>
	</div>
	
	
	<?php if( $video ) : ?>
	<h2><?php echo $video->video_title; ?></h2>
<div id="video_wrapper">
<div id="video_holder">Loading the player...</div>
<script type="text/javascript">
    jwplayer("video_holder").setup({
        file: "<?php echo $video->video_url; ?>",
        image: "<?php echo $video->video_image; ?>",
        width: 635,
        height: 400
    });
</script>
</div>
<p><?php echo $video->video_description; ?></p>
<?php if( $tags ) { ?>
<p><strong>Tags:</strong>
<?php 
$videoss = array();
 foreach($tags as $tag) { 
	$hashtags[] = $tag->tag_name;
	$videoss[] = "<a href=\"". site_url("tag/" . $tag->tag_slug ) . "\">{$tag->tag_name}</a>";
 } 
 echo implode(", ", $videoss);
 ?>
	</p>
<?php } ?>

<div class="btn-group btn-group-justified">
  <a class="btn btn-lg btn-default btn-facebook" href="https://www.facebook.com/sharer/sharer.php?u=<?php echo urlencode( current_url() ); ?>" target="_blank">Share on Facebook</a>
  <a class="btn btn-lg btn-default btn-twitter" href="http://twitter.com/share?text=<?php echo urlencode($video->video_title); ?>&url=<?php echo urlencode( current_url() ); ?>&hashtags=<?php echo implode(',', $hashtags); ?>" target="_blank">Tweet this Video</a>
  <a class="btn btn-lg btn-default btn-google" href="https://plus.google.com/share?url=<?php echo urlencode( current_url() ); ?>" target="_blank">Share on Google+</a>
</div>

<div id="comment_wrapper">
	

<!--	<div class="fb-comments" data-href="<?php echo current_url(); ?>" data-width="750" data-numposts="10" data-colorscheme="light"></div>	-->
 <div id="disqus_thread"></div>
    <script type="text/javascript">
        /* * * CONFIGURATION VARIABLES: EDIT BEFORE PASTING INTO YOUR WEBPAGE * * */
        var disqus_shortname = 'tagskie'; // required: replace example with your forum shortname

        /* * * DON'T EDIT BELOW THIS LINE * * */
        (function() {
            var dsq = document.createElement('script'); dsq.type = 'text/javascript'; dsq.async = true;
            dsq.src = '//' + disqus_shortname + '.disqus.com/embed.js';
            (document.getElementsByTagName('head')[0] || document.getElementsByTagName('body')[0]).appendChild(dsq);
        })();
    </script>
    <!--<noscript>Please enable JavaScript to view the <a href="http://disqus.com/?ref_noscript">comments powered by Disqus.</a></noscript>
    <a href="http://disqus.com" class="dsq-brlink">comments powered by <span class="logo-disqus">Disqus</span></a>-->
    
    
</div><!-- #comment_wrapper -->

<?php endif; ?>
	</div>
<div class="col-md-4">
	


	<div class="ads" style="margin-bottom:20px">
	<script async src="//pagead2.googlesyndication.com/pagead/js/adsbygoogle.js"></script>
<!-- Tagskie.Net VIS 2 -->
<ins class="adsbygoogle"
     style="display:inline-block;width:300px;height:250px"
     data-ad-client="ca-pub-7233800271694028"
     data-ad-slot="6309898121"></ins>
<script>
(adsbygoogle = window.adsbygoogle || []).push({});
</script>
	</div>
	
<div class="panel panel-default">
	<div class="panel-heading"><h3 class="panel-title">Find Us on Facebook</h3></div>
  <div class="panel-body">
		<div class="fb-like-box" data-href="https://www.facebook.com/alchienetcafe" data-width="270" data-colorscheme="light" data-show-faces="true" data-header="false" data-stream="false" data-show-border="false"></div>
  </div>
</div>
	

<div class="panel panel-default">
  <div class="panel-heading"><h3 class="panel-title">Receive our Latests Videos</h3></div>
  <div class="panel-body" id="related-videos">
	  
<!-- Begin MailChimp Signup Form -->
<div id="mc_embed_signup">
<form action="http://angeliwebs.us2.list-manage1.com/subscribe/post?u=eedf27f5cbf1aa4464ce466eb&amp;id=d660452920" method="post" id="mc-embedded-subscribe-form" name="mc-embedded-subscribe-form" class="validate" target="_blank" novalidate>
	
<div class="form-group">
	<input type="email" required value="" name="EMAIL" class="required email form-control" id="mce-EMAIL" placeholder="Your Email Address">
	</div>

	<div id="mce-responses" class="clear">
		<div class="response" id="mce-error-response" style="display:none"></div>
		<div class="response" id="mce-success-response" style="display:none"></div>
	</div>    <!-- real people should not fill this in and expect good things - do not remove this or risk form bot signups-->
    <div style="position: absolute; left: -5000px;"><input type="text" name="b_eedf27f5cbf1aa4464ce466eb_d660452920" tabindex="-1" value=""></div>
    <div class="clear"><input type="submit" value="Subscribe" name="subscribe" id="mc-embedded-subscribe" class="button btn btn-primary"></div>
</form>
</div>

<!--End mc_embed_signup-->

	</div>
</div>


<div class="panel panel-default">
  <div class="panel-heading"><h3 class="panel-title">Related Videos</h3></div>
  <div class="panel-body" id="related-videos">
	  
<center><img src="<?php echo base_url() . "assets/images/ajax-loader.gif"; ?>"></center>

	</div>
</div>
	
	
			<div class="ads">
			
			<script async src="//pagead2.googlesyndication.com/pagead/js/adsbygoogle.js"></script>
<!-- Tagskie.Net VIS 1 -->
<ins class="adsbygoogle"
     style="display:inline-block;width:300px;height:600px"
     data-ad-client="ca-pub-7233800271694028"
     data-ad-slot="7310783321"></ins>
<script>
(adsbygoogle = window.adsbygoogle || []).push({});
</script>

			</div>	
	
	
</div>
        
        </div>
        
      </div>
</div> <!-- /container -->
<?php $this->load->view('overall_footer'); ?>
