<?php $this->load->view('overall_header'); ?>
<div class="container homepage" id="frontpage">
        <div class="main">
   
   <div class="well">
   <div class="row">
	   <div class="col-md-8">
			<h2><?php echo $tag_name; ?> Videos</h2>
	   </div>
	   <div class="col-md-4">
		   
		   <div class="col-md-3">
		<div class="fb-like" data-href="<?php echo current_url(); ?>" data-layout="box_count" data-action="like" data-show-faces="true" data-share="false"></div>
	</div>
	
	<div class="col-md-3">
		<a href="https://twitter.com/share" class="twitter-share-button" data-url="<?php echo current_url(); ?>" data-via="chesteralan" data-lang="en" data-related="Tagskie.Net" data-count="vertical">Tweet</a>
<script>!function(d,s,id){var js,fjs=d.getElementsByTagName(s)[0];if(!d.getElementById(id)){js=d.createElement(s);js.id=id;js.src="https://platform.twitter.com/widgets.js";fjs.parentNode.insertBefore(js,fjs);}}(document,"script","twitter-wjs");</script>
	</div>
	
	<div class="col-md-3">
	<!-- Place this tag where you want the +1 button to render. -->
<div class="g-plusone" data-size="tall" data-count="true" data-annotation="bubble" data-width="300" data-href="<?php echo current_url(); ?>"></div>
	</div>
    
    <div class="col-md-3">
<script src="//platform.linkedin.com/in.js" type="text/javascript">lang: en_US</script>
<script type="IN/Share" data-url="<?php echo current_url(); ?>" data-counter="top"></script>
		</div>
		   
		   
	   </div>
   </div>
   </div>
   
      <div class="row">

<?php 
$n = 0;
if( $videos ) foreach($videos as $video) : 

?>
  <div class="col-sm-4 col-md-3">
    <div class="thumbnail">
      <a href="<?php echo site_url( "watch/" .  $video->video_slug); ?>">
      <?php $image = $video->video_image; 
      if( $video->video_image == '' ) {
		  switch($video->video_source) {
			  case 'youtube':
				$image = '//i.ytimg.com/vi/'.youtube_id( $video->video_url ).'/0.jpg';
			  break;
			  default:
				$image = 'http://www.balaniinfotech.com/wp-content/themes/balani/images/noimage.jpg';
			  break;
		  }
	  }  
      ?>
      <img src="<?php echo $image; ?>" alt="<?php echo $video->video_title; ?>">
      </a>
      <div class="caption text-center">
        <a href="<?php echo site_url( "watch/" . $video->video_slug); ?>"><h3><?php echo $video->video_title; ?></h3></a>
      </div>
    </div>
  </div>
<?php
$n++;
if( $n == 4 ) {
	echo "</div><div class='row'>";
	$n = 0;
}
endforeach; ?>
        
        </div>
        
      </div>
</div> <!-- /container -->
<?php $this->load->view('overall_footer'); ?>
