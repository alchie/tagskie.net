<?php $this->load->view('overall_header'); ?>
<div class="container" id="login">

      <div class="form-signin">


  <div class="panel panel-default">
 
  <div class="panel-heading">
    <h3 class="panel-title">log in</h3>
  </div>

 <div class="panel-body">


<?php if ( $alert ) { ?>
<div class="alert alert-<?php echo $alert_status; ?> alert-dismissable">
  <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
  <?php echo $alert_message; ?>
  </div>
<?php } ?>


      <?php echo form_open( uri_string() ); ?>
       
        <input type="hidden" name="redirect" value="<?php echo $redirect_url; ?>" />
        
        

        <p><input name="email" type="email" class="form-control" placeholder="Email address" required autofocus>
        </p>
        <p><input name="password" type="password" class="form-control" placeholder="Password" required>
        </p>

       <p> <button class="btn btn-lg btn-success btn-block" type="submit">Log in</button></p>


      </form>
      
       

 </div> <!-- .panel-body -->
  
</div>  <!-- .panel -->
 
 
</div>


</div> <!-- /container -->
<?php $this->load->view('overall_footer'); ?>
