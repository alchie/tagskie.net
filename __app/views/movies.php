<?php $this->load->view('overall_header'); ?>
<div class="container homepage" id="frontpage">
        <div class="main">
   
      <div class="row">

<?php 
$n = 0;
if( $movies ) foreach($movies as $movie) : 

?>
  <div class="col-sm-3 col-md-2">
    <div class="thumbnail">
      <a href="<?php echo site_url( "movie/" .  $movie->movie_year . '/' . $movie->movie_slug); ?>">
      <img src="<?php echo $movie->movie_poster; ?>" alt="<?php echo $movie->movie_title; ?>">
      </a>
    </div>
  </div>
<?php
$n++;
if( $n == 6 ) {
	echo "</div><div class='row'>";
}
endforeach; ?>
        
        </div>
        
      </div>
</div> <!-- /container -->
<?php $this->load->view('overall_footer'); ?>
