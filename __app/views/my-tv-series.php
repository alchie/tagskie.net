<?php $this->load->view('overall_header'); ?>
<div class="container homepage" id="frontpage">
        <div class="main">
   
      <div class="row">

<div class="col-md-12">

<ul class="nav nav-tabs nav-justified">
  <li><a href="<?php echo site_url('my/posts'); ?>">Videos</a></li>
  <li><a href="<?php echo site_url('my/movies'); ?>">Movies</a></li>
  <li class="active"><a href="<?php echo site_url('my/tv_series'); ?>" style="background-color: #fcf8e3; border-color: #faebcc;">TV Series</a></li>
</ul>

<div class="well ">

<div class="panel panel-warning">
  <div class="panel-heading">
	  <p><a href="<?php echo site_url('my/tv_series/add'); ?>" class="btn btn-primary btn-sm pull-right">Add TV Series</a></p>
    <h3 class="panel-title">My TV Series</h3>
  </div>
  <div class="panel-body">
    
    
    <table class="table table-striped">
        <thead>
          <tr>
            <th>#</th>
            <th>First Name</th>
            <th>Last Name</th>
            <th>Username</th>
          </tr>
        </thead>
        <tbody>
          <tr>
            <td>1</td>
            <td>Mark</td>
            <td>Otto</td>
            <td>@mdo</td>
          </tr>
          <tr>
            <td>2</td>
            <td>Jacob</td>
            <td>Thornton</td>
            <td>@fat</td>
          </tr>
          <tr>
            <td>3</td>
            <td>Larry</td>
            <td>the Bird</td>
            <td>@twitter</td>
          </tr>
        </tbody>
      </table>
    
    
  </div>
</div>

</div>

</div>
        
        </div>
        
      </div>
</div> <!-- /container -->
<?php $this->load->view('overall_footer'); ?>
