<?php $this->load->view('overall_header'); ?>
<?php $hashtags = array('Tagskie.Net'); ?>
<div class="container">
        <div class="main">

      <div class="row">

			<div class="col-md-8">
				<div class="panel panel-default">
  <div class="panel-heading"><h3 class="panel-title"><?php echo $movie->movie_title; ?> (<?php echo $movie->movie_year; ?>)</h3></div>
  <div class="panel-body">
	  <div class="col-md-4">
		<img src="<?php echo $movie->movie_poster; ?>" width="100%">
	  </div>
	  <div class="col-md-8">
	  <p><?php echo $movie->movie_description; ?></p>
		  <?php echo ($movie->movie_imdb != '') ? '<p><a href="'.$movie->movie_imdb.'" target="_blank">'.$movie->movie_imdb.'</a></p>' : ''; ?>
		 <?php if( $genres ) { ?>
			 <p><strong>Genre:</strong> 
			 <?php $genress = array(); 
				foreach($genres as $genre) { 
					$genress[] = "<a href=\"" . site_url("movie/genre/". $genre->genre_slug ) ."\">{$genre->genre_name}</a>";
				} 
				echo implode(", ", $genress);
				?>
				</p>
		<?php } ?>
		 <?php if( $tags ) { ?>
			 <p><strong>Tags:</strong> 
			 <?php $tagss = array(); 
				foreach($tags as $tag) { 
					$hashtags[] = $tag->tag_name;
					$tagss[] = "<a href=\"".site_url("movie/tag/".$tag->tag_slug)."\">{$tag->tag_name}</a>";
				} 
				echo implode(", ", $tagss);
				?>
				</p>
		<?php } ?>
	</div>
	</div>
</div>



<div class="row">

<?php 
$n = 0;
if( $videos ) foreach($videos as $video) : 

?>
  <div class="col-sm-4 col-md-3">
    <div class="thumbnail">
      <a href="<?php echo site_url( "watch/" .  $video->video_slug); ?>">
      <?php $image = $video->video_image; 
      if( $video->video_image == '' ) {
		  switch($video->video_source) {
			  case 'youtube':
				$image = '//i.ytimg.com/vi/'.youtube_id( $video->video_url ).'/0.jpg';
			  break;
			  default:
				$image = 'http://www.balaniinfotech.com/wp-content/themes/balani/images/noimage.jpg';
			  break;
		  }
	  }  
      ?>
      <img src="<?php echo $image; ?>" alt="<?php echo $video->video_title; ?>">
      </a>
    </div>
  </div>
<?php
$n++;
if( $n == 4 ) {
	echo "</div><div class='row'>";
}
endforeach; ?>
        
        </div>





			</div>
			<div class="col-md-4">
				
				
	<div class="ads" style="margin-bottom:20px">
	<script async src="//pagead2.googlesyndication.com/pagead/js/adsbygoogle.js"></script>
<!-- Tagskie.Net VIS 2 -->
<ins class="adsbygoogle"
     style="display:inline-block;width:300px;height:250px"
     data-ad-client="ca-pub-7233800271694028"
     data-ad-slot="6309898121"></ins>
<script>
(adsbygoogle = window.adsbygoogle || []).push({});
</script>
	</div>
				
				
				<div class="panel panel-default">
  <div class="panel-body">
    <div class="col-md-3">
		<div class="fb-like" data-href="<?php echo current_url(); ?>" data-layout="box_count" data-action="like" data-show-faces="true" data-share="false"></div>
	</div>
	
	<div class="col-md-3">
		<a href="https://twitter.com/share" class="twitter-share-button" data-url="<?php echo current_url(); ?>" data-via="chesteralan" data-lang="en" data-related="Tagskie.Net" data-count="vertical">Tweet</a>
<script>!function(d,s,id){var js,fjs=d.getElementsByTagName(s)[0];if(!d.getElementById(id)){js=d.createElement(s);js.id=id;js.src="https://platform.twitter.com/widgets.js";fjs.parentNode.insertBefore(js,fjs);}}(document,"script","twitter-wjs");</script>
	</div>
	
	<div class="col-md-3">
	<!-- Place this tag where you want the +1 button to render. -->
<div class="g-plusone" data-size="tall" data-count="true" data-annotation="bubble" data-width="300" data-href="<?php echo current_url(); ?>"></div>
	</div>
    
    <div class="col-md-3">
<script src="//platform.linkedin.com/in.js" type="text/javascript">lang: en_US</script>
<script type="IN/Share" data-url="<?php echo current_url(); ?>" data-counter="top"></script>
		</div>
  </div>
</div>
	

<div class="panel panel-default">
  <div class="panel-heading"><h3 class="panel-title">Receive our Latests Videos</h3></div>
  <div class="panel-body" id="related-videos">
	  
<!-- Begin MailChimp Signup Form -->
<div id="mc_embed_signup">
<form action="http://angeliwebs.us2.list-manage1.com/subscribe/post?u=eedf27f5cbf1aa4464ce466eb&amp;id=d660452920" method="post" id="mc-embedded-subscribe-form" name="mc-embedded-subscribe-form" class="validate" target="_blank" novalidate>
	
<div class="form-group">
	<input type="email" required value="" name="EMAIL" class="required email form-control" id="mce-EMAIL" placeholder="Your Email Address">
	</div>

	<div id="mce-responses" class="clear">
		<div class="response" id="mce-error-response" style="display:none"></div>
		<div class="response" id="mce-success-response" style="display:none"></div>
	</div>    <!-- real people should not fill this in and expect good things - do not remove this or risk form bot signups-->
    <div style="position: absolute; left: -5000px;"><input type="text" name="b_eedf27f5cbf1aa4464ce466eb_d660452920" tabindex="-1" value=""></div>
    <div class="clear"><input type="submit" value="Subscribe" name="subscribe" id="mc-embedded-subscribe" class="button btn btn-primary"></div>
</form>
</div>

<!--End mc_embed_signup-->

	</div>
</div>
			
				<div class="ads" style="margin-bottom:20px">
	<script async src="//pagead2.googlesyndication.com/pagead/js/adsbygoogle.js"></script>
<!-- Tagskie.Net VIS 2 -->
<ins class="adsbygoogle"
     style="display:inline-block;width:300px;height:250px"
     data-ad-client="ca-pub-7233800271694028"
     data-ad-slot="6309898121"></ins>
<script>
(adsbygoogle = window.adsbygoogle || []).push({});
</script>
	</div>
				

				
			</div>
		</div>
        
      </div>
</div> <!-- /container -->
<?php $this->load->view('overall_footer'); ?>
