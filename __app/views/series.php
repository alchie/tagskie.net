<?php $this->load->view('overall_header'); ?>
<div class="container homepage" id="frontpage">
        <div class="main">
   
      <div class="row">

<?php 
$n = 0;
if( $series ) foreach($series as $serie) : 

?>
  <div class="col-sm-3 col-md-3">
    <div class="thumbnail">
      <a href="<?php echo site_url( "tv-series/" .  $serie->series_slug ); ?>">
      <img src="<?php echo $serie->series_poster; ?>" alt="<?php echo $serie->series_title; ?>">
      </a>
      <div class="caption text-center">
        <a href="<?php echo site_url( "tv-series/" .  $serie->series_slug ); ?>"><h3><?php echo $serie->series_title; ?></h3></a>
      </div>
    </div>
  </div>
<?php
$n++;
if( $n == 4 ) {
	echo "</div><div class='row'>";
}
endforeach; ?>
        
        </div>
        
      </div>
</div> <!-- /container -->
<?php $this->load->view('overall_footer'); ?>
