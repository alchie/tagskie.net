<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class MY_Controller extends CI_Controller {
        
    function __construct() 
    {
        parent::__construct();
                
        // add core packages
        $this->load->add_package_path("../indavao.net/__common");
        
        // helpers
        $this->load->helper( array('url', 'cookie', 'youtube') );
        
        // libraries
        $this->load->library('template_data');
        
        $this->template_data->opengraph();
		
        if( $this->session->userdata('logged_in') )
        {
            $this->template_data->set('current_session', $this->session->all_userdata() );
        } else {
            $this->template_data->set('current_session', array() );
        }
        
        $this->template_data->set('main_page', 'home' );
	    $this->template_data->set('sub_page', 'home' );

        $this->config->load('facebook');
        $this->template_data->set('fbAppId', $this->config->item('appId') );
        $this->template_data->set('fbScope', $this->config->item('scope') );
        $this->template_data->set('fbPageId', $this->config->item('pageId') );
        
        $this->template_data->set('referrer', get_cookie('referral_id') );
        
        
    }
    
    public function _mustLogin($url='my/posts', $r=FALSE)
    {
        if ( !  $this->session->userdata('logged_in') ) {
            if ($r===TRUE) {
                redirect('login' , 'refresh');
            } else {
                redirect('login' , 'location', 301);
            }
        }
    }
    
    public function _notLoggedIn() {
        if( $this->session->userdata('logged_in') === TRUE ) {
            redirect('my/posts', 'location', 301);
            exit;
        } 
        return true;
        
    }
    
}
