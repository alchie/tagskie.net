<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Series extends MY_Controller {

	public function __construct() {
		parent::__construct();
		$this->load->model( array('Tv_series_model', 'Tv_series_videos_model', 'Tv_series_genre_model', 'Tv_series_tags_model', 'Videos_model', 'Videos_tag_model') );
	}
	
	public function index()
	{
		$this->template_data->set('main_page', 'series' );

		$this->template_data->page_title('TV Series');
		
		$this->Tv_series_model->setSeriesActive(1, true);
		$this->template_data->set( 'series', $this->Tv_series_model->populate() );

		
		$this->load->view('series', $this->template_data->get() );
		
	}
	
	function watch($slug)
	{
		$this->template_data->set('main_page', 'series' );
		$this->template_data->set('sub_page', 'watch' );
		
		$this->Tv_series_model->setSeriesSlug($slug, true);
		
		if( $this->Tv_series_model->nonEmpty() === TRUE ) {
			$series = $this->Tv_series_model->getResults();
			$this->template_data->set('series', $series );
			$this->template_data->page_title( $series->series_title );
			$this->template_data->opengraph( array(
				'title'=> $series->series_title,
				'description'=>$series->series_description,
				'image'=> $series->series_poster,
				'image:width'=>'289',
				'image:height'=>'102',
			));

			$this->Videos_tag_model->setJoin('videos', 'videos.video_id = videos_tag.video_id');
			$this->Videos_tag_model->setFilter('videos_tag.tag_id', $series->series_tag);
			$this->Videos_tag_model->setFilter('videos.video_active', 1);
			$this->Videos_tag_model->setOrder('videos.video_priority', 'DESC');
			$this->template_data->set( 'videos', $this->Videos_tag_model->populate() );
			
			$this->Tv_series_genre_model->setJoin('taxonomy_genre', 'taxonomy_genre.genre_id = tv_series_genre.genre_id');
			$this->Tv_series_genre_model->setFilter('tv_series_genre.series_id', $series->series_id);
			$this->template_data->set( 'genres', $this->Tv_series_genre_model->populate() );

			$this->Tv_series_tags_model->setJoin('taxonomy_tags', 'taxonomy_tags.tag_id = tv_series_tags.tag_id');
			$this->Tv_series_tags_model->setFilter('tv_series_tags.series_id', $series->series_id);
			$this->template_data->set( 'tags', $this->Tv_series_tags_model->populate() );
			
			$body_view = 'series_watch';
			
		} else {
			
			$this->template_data->page_title( "404 Error - Page Not Found!" );			
			$this->Videos_model->clearWhere();
			$this->Videos_model->setOrder('video_added', 'DESC');
			$this->template_data->set( 'videos', $this->Videos_model->populate() );
			$body_view = '404';
		}
		
		
		$this->load->view($body_view, $this->template_data->get() );
		
	}
	
	public function tag($slug)
	{
		$this->template_data->set('main_page', 'series' );

		$this->template_data->page_title('TV Series');
		
		$this->Tv_series_model->setSeriesActive(1, true);
		$this->template_data->set( 'series', $this->Tv_series_model->populate() );

		
		$this->load->view('series', $this->template_data->get() );
		
	}
	
	public function genre($slug)
	{
		$this->template_data->set('main_page', 'series' );

		$this->template_data->page_title('TV Series');
		
		$this->Tv_series_model->setSeriesActive(1, true);
		$this->template_data->set( 'series', $this->Tv_series_model->populate() );

		
		$this->load->view('series', $this->template_data->get() );
		
	}
}

/* End of file welcome.php */
/* Location: ./application/controllers/welcome.php */
