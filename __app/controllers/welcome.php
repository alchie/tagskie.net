<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Welcome extends MY_Controller {

	public function index()
	{
		$this->template_data->set('main_page', 'frontpage' );
		
			$this->load->model( array('Videos_model') );
			$this->Videos_model->setVideoActive(1, true);
			$this->Videos_model->setOrder('video_added', 'DESC');
			$this->template_data->set( 'videos', $this->Videos_model->populate() );
		
		$this->load->view('main_frontpage', $this->template_data->get() );
	}
	
	public function terms_of_service()
	{
		$this->load->view('terms_of_service', $this->template_data->get() );
	}
	
	public function privacy_policy()
	{
		$this->load->view('privacy_policy', $this->template_data->get() );
	}
	
	public function login() {
		
		$this->_notLoggedIn();
		$this->load->helper( array('form') );
		$this->load->library( array('form_validation' ) );
		$this->form_validation->set_rules('email', 'Email', 'trim|required|valid_email|xss_clean');
		$this->form_validation->set_rules('password', 'Password', 'trim|required|md5|sha1');
		
		if ($this->form_validation->run() == FALSE)
		{
		    if( $this->input->post() ) {
		         $this->template_data->alert( validation_errors(), 'danger');
		    }
		} 
		else 
		{
			$this->load->model('Users_model');
		    $this->Users_model->setEmail( $this->input->post('email'), true );
		    $this->Users_model->setPassword( $this->input->post('password'), true );
		    
		    
			if( $this->Users_model->nonEmpty() === TRUE ) {
		        $current_user = $this->Users_model->getResults();
		        $this->session->set_userdata( array(
					'user_id' => $current_user->id,
					'username' => $current_user->username,
					'logged_in' => true,
				) );
				redirect('my/posts', 'refresh'); 
		    }
		}
		
		$this->load->view('login', $this->template_data->get() );
		
	}
}

/* End of file welcome.php */
/* Location: ./application/controllers/welcome.php */
