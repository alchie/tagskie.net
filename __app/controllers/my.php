<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class My extends MY_Controller {

	public function __construct() {
		parent::__construct();
		$this->_mustLogin();
	}
	
	public function index() {
		$this->posts();
	}
	
	public function posts() {
		$this->load->view('my-posts', $this->template_data->get() );
	}
	
	public function movies() {
		$this->load->view('my-movies', $this->template_data->get() );
	}
	
	public function tv_series() {
		$this->load->view('my-tv-series', $this->template_data->get() );
	}
	
	public function logout() {
		$this->session->sess_destroy();
		redirect('login', 'refresh'); 
	}
}

/* End of file welcome.php */
/* Location: ./application/controllers/welcome.php */
