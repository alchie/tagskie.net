<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Movies extends MY_Controller {

	public function __construct() {
		parent::__construct();
		$this->load->model( array('Movies_model', 'Movies_videos_model', 'Movies_genre_model', 'Movies_tags_model', 'Videos_model', 'Videos_tag_model') );
	}
	
	public function index()
	{
		
		$this->template_data->set('main_page', 'movies' );

		$this->template_data->page_title('Movies');
		
		$this->Movies_model->setMovieActive(1, true);
		$this->template_data->set( 'movies', $this->Movies_model->populate() );

		
		$this->load->view('movies', $this->template_data->get() );
		
	}
	
	public function watch($year, $slug)
	{
		$this->template_data->set('main_page', 'movies' );
		
		$this->Movies_model->setMovieYear($year, true);
		$this->Movies_model->setMovieSlug($slug, true);
		
		if( $this->Movies_model->nonEmpty() === TRUE ) {
			$body_view = 'movie_watch';
			$movie = $this->Movies_model->getResults();
			$this->template_data->page_title( "{$movie->movie_title} ({$movie->movie_year})" );
			$this->template_data->set( 'movie', $movie );
			
			$this->template_data->opengraph( array(
				'title'=> "{$movie->movie_title} ({$movie->movie_year})",
				'description'=>$movie->movie_description,
				'image'=> $movie->movie_poster,
				'image:width'=>'289',
				'image:height'=>'102',
			));
			
			$this->Videos_tag_model->setJoin('videos', 'videos.video_id = videos_tag.video_id');
			$this->Videos_tag_model->setFilter('videos_tag.tag_id', $movie->movie_tag);
			$this->Videos_tag_model->setFilter('videos.video_active', 1);
			$this->Videos_tag_model->setOrder('videos.video_priority', 'DESC');
			$this->template_data->set( 'videos', $this->Videos_tag_model->populate() );
			
			$this->Movies_genre_model->setJoin('taxonomy_genre', 'taxonomy_genre.genre_id = movies_genre.genre_id');
			$this->Movies_genre_model->setFilter('movies_genre.movie_id', $movie->movie_id);
			$this->template_data->set( 'genres', $this->Movies_genre_model->populate() );
			
			$this->Movies_tags_model->setJoin('taxonomy_tags', 'taxonomy_tags.tag_id = movies_tags.tag_id');
			$this->Movies_tags_model->setFilter('movies_tags.movie_id', $movie->movie_id);
			$this->template_data->set( 'tags', $this->Movies_tags_model->populate() );
			
		} else {
			
			$this->template_data->page_title( "404 Error - Page Not Found!" );			
			$this->Videos_model->clearWhere();
			$this->Videos_model->setOrder('video_added', 'DESC');
			$this->template_data->set( 'videos', $this->Videos_model->populate() );
			$body_view = '404';
			
		}

		
		$this->load->view($body_view, $this->template_data->get() );
		
	}
	
	public function tag($slug)
	{
		$this->template_data->set('main_page', 'movies' );

		$this->template_data->page_title('Movies');
		
		$this->Movies_model->setMovieActive(1, true);
		$this->template_data->set( 'movies', $this->Movies_model->populate() );

		
		$this->load->view('movies', $this->template_data->get() );
		
	}
	
	public function genre($slug)
	{
		$this->template_data->set('main_page', 'movies' );

		$this->template_data->page_title('Movies');
		
		$this->Movies_model->setMovieActive(1, true);
		$this->template_data->set( 'movies', $this->Movies_model->populate() );

		
		$this->load->view('movies', $this->template_data->get() );
		
	}
	
}

/* End of file welcome.php */
/* Location: ./application/controllers/welcome.php */
