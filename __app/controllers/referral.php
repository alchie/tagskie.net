<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Referral extends MY_Controller {

	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -  
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in 
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see http://codeigniter.com/user_guide/general/urls.html
	 */
	public function index($user_id, $redirect=NULL)
	{
	    $this->setCookie($user_id, $redirect);
	}
	
	public function id($user_id, $redirect=NULL) {
	    $this->setCookie($user_id, $redirect);
	}
	
	private function setCookie($user_id, $redirect=NULL) {
	    
	    $this->load->helper('cookie');
	    
	    set_cookie( 'referral_id', $user_id, '86500' );
	    
	    /* 
	    if( $redirect == NULL ) {
	        $redirect = 'account/signup';
	    }
	    
	    redirect($redirect, 'refresh');
	    exit;
	    */
        $this->load->helper('form');		
		$this->load->view('overall_header', $this->template_data->get() );
		$this->load->view('main_frontpage', $this->template_data->get() );
		$this->load->view('overall_footer', $this->template_data->get() );
	}
	
}

/* End of file welcome.php */
/* Location: ./application/controllers/welcome.php */
