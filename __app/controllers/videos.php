<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Videos extends MY_Controller {

	public function __construct() {
		parent::__construct();
		$this->load->model( array('Videos_model', 'Videos_tag_model', 'Taxonomy_tags_model') );
	}
	
	public function index()
	{
		$this->template_data->set('main_page', 'videos' );

		$this->Videos_model->setVideoActive(1, true);
		$this->Videos_model->setOrder('video_added', 'DESC');
		$this->template_data->set( 'videos', $this->Videos_model->populate() );

		
		$this->load->view('main_frontpage', $this->template_data->get() );
		
	}
	
	public function watch($slug)
	{
		$this->template_data->set('main_page', 'videos' );
		$this->template_data->set('sub_page', 'watch' );
		
		
		$this->Videos_model->setVideoSlug($slug, true);
		
		if( $this->Videos_model->nonEmpty() === TRUE ) {
			$video = $this->Videos_model->getResults();
			$this->template_data->set( 'video', $video );
			$this->template_data->page_title( $video->video_title );
			$this->template_data->opengraph( array(
				'title'=> $video->video_title,
				'description'=>$video->video_description,
				'type'=>'video.other',
				'image'=> $video->video_image,
				'image:width'=>'289',
				'image:height'=>'102',
			));
			$this->Videos_tag_model->setJoin('taxonomy_tags', 'taxonomy_tags.tag_id = videos_tag.tag_id');
			/*
			$this->Videos_tag_model->setJoin('videos', 'videos.video_id = videos_tag.video_id');
			$this->Videos_tag_model->setOrder('video_added', 'DESC');
			$this->template_data->set( 'related_videos', $this->Videos_tag_model->populate() );
			$this->Videos_tag_model->setFilter('videos_tag.video_id', $video->video_id);
			*/
			$this->Videos_tag_model->setVideoId($video->video_id, true);
			$this->template_data->set( 'tags', $this->Videos_tag_model->populate() );
			
			$body_view = 'video_watch';
			
		} else {
			
			$this->template_data->page_title( "404 Error - Page Not Found!" );			
			$this->Videos_model->clearWhere();
			$this->Videos_model->setOrder('video_added', 'DESC');
			$this->template_data->set( 'videos', $this->Videos_model->populate() );
			$body_view = '404';
			
		}
		
		
		$this->load->view($body_view, $this->template_data->get() );
		
	}
	
	public function tag($slug)
	{
		$this->template_data->set('main_page', 'videos' );
		$this->template_data->set('sub_page', 'tag' );
		
		$this->Videos_tag_model->setJoin('taxonomy_tags', 'taxonomy_tags.tag_id = videos_tag.tag_id');
		$this->Videos_tag_model->setJoin('videos', 'videos.video_id = videos_tag.video_id');
		$this->Videos_tag_model->setFilter('taxonomy_tags.tag_slug', $slug);
		$videos = $this->Videos_tag_model->populate();
		$this->template_data->set( 'videos', $videos );
		
		if( isset( $videos[0]->tag_name ) ) {
			$this->template_data->set( 'tag_name', $videos[0]->tag_name );
			$this->template_data->set( 'tag_slug', $videos[0]->tag_slug );
			$this->template_data->page_title( 'Watch `' .$videos[0]->tag_name . "` Videos at Tagskie.Net" );
			$this->template_data->opengraph( array(
				'title'=> 'Watch `' .$videos[0]->tag_name . "` Videos at Tagskie.Net",
				'description'=> 'Watch ' . $videos[0]->tag_name . ' Videos, Watch ' . $videos[0]->tag_name . ' TV Series, Watch ' . $videos[0]->tag_name . ' Movies',
			));
		}

		
		$this->load->view('videos', $this->template_data->get() );
		
	}
	
}

/* End of file welcome.php */
/* Location: ./application/controllers/welcome.php */
