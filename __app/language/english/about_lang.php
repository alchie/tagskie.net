<?php
/******************************************************************************/
/*  Company */  
/******************************************************************************/
$lang['company-overview-title'] = 'Company Overview';
$lang['company-partnerships-title'] = 'Company Partners';
$lang['company-team-title'] = 'Company Staff';

/******************************************************************************/
/*  Services */  
/******************************************************************************/
$lang['services-ads-title'] = 'Online Advertising Packages';
$lang['services-logo-title'] = 'Logo Design Packages';
$lang['services-graphics-title'] = 'Graphics Design Packages';
$lang['services-webdev-title'] = 'Web Design &amp; Development Packages';
$lang['services-hosting-title'] = 'Web Hosting Packages';
$lang['services-domain-title'] = 'Domain Management Packages';
$lang['services-seo-title'] = 'SEO Packages';

/******************************************************************************/
/*  Legal */  
/******************************************************************************/
$lang['legal-user_agreement-title'] = 'User Agreement';
$lang['legal-privacy_policy-title'] = 'Privacy Policy';
$lang['legal-cookie_policy-title'] = 'Cookie Policy';
$lang['legal-terms_of_use-title'] = 'Terms of Use';

/******************************************************************************/
/*  others */  
/******************************************************************************/
$lang['faqs-title'] = 'Frequently Asked Questions';
$lang['sitemap-title'] = 'Sitemap';
$lang['support-title'] = 'Get Support';
$lang['contact-title'] = 'Contact Us';
