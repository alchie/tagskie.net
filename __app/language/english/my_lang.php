<?php

/******************************************************************************/
/*  Fix */  
/******************************************************************************/
$lang['urgent-title'] = 'Urgent Attention!';
$lang['fix-add-email-success'] = 'Email Successfully Added!';
$lang['fix-add-email-error'] = 'Email is already set!';
$lang['fix-add-password-success'] = 'Password Successfully Added!';
$lang['fix-add-password-error'] = 'Password is already set!';
$lang['fix-verify-email-success'] = 'Email Successfully Verified!';
$lang['fix-verify-email-error'] = 'Code error!';
/******************************************************************************/
/*  Dashboard */  
/******************************************************************************/
$lang['dashboard-title'] = 'My Dashboard';



/******************************************************************************/
/*  Posts */  
/******************************************************************************/
// Posts - Business Listings
$lang['listings-business-title'] = 'Business';
// Posts - Real Estate
$lang['listings-real-estate-title'] = 'Real Estate';
// Posts - Job Posts
$lang['listings-job-posts-title'] = 'Job Posts';
// Posts - Services
$lang['listings-services-title'] = 'Services';
// Posts - Cars
$lang['listings-cars-title'] = 'Cars';
// Posts - Motorcycles
$lang['listings-motorcycles-title'] = 'Motorcycles';
// Posts - Events
$lang['listings-events-title'] = 'Events';

/******************************************************************************/
/*  Network */  
/******************************************************************************/
// Network - Connections
$lang['network-earnings-title'] = 'My Earnings';
// Network - Friends
$lang['network-points-title'] = 'My Points';
// Network - Referrals
$lang['network-referrals-title'] = 'My Referrals';
// Network - Heirarchy
$lang['network-hierarchy-title'] = 'My Hierarchy';

//network-hierarchy-assignment-success
$lang['network-hierarchy-assignment-success'] = 'Successfully Assigned Referrals!';

/******************************************************************************/
/*  Account */  
/******************************************************************************/
$lang['account-default-error'] = 'Submission Error!';

// Account - Resume
$lang['account-resume-title'] = 'Resume / CV';

// Account - Business Profile
$lang['account-business-profile-title'] = 'Business Profile';

// Account - Profile 
$lang['account-profile-title'] = 'Profile';
$lang['account-profile-alert-success'] = 'Profile Updated!';
$lang['account-profile-alert-fail'] = 'Unable to update profile!';

// Account - Account Settings
$lang['account-account-settings-title'] = 'Account Settings';
$lang['account-account-settings-alert-success'] = 'Account Settings Updated!';
$lang['account-account-settings-alert-fail'] = 'Unable to update account settings!';
$lang['account-account-settings-username-exists'] = 'Referral ID is already taken!';


// Account - Change Password
$lang['account-change-password-title'] = 'Change Password';
$lang['account-change-password-alert-success'] = 'Password Successfully Changed!';
$lang['account-change-password-alert-fail'] = 'Unable to change password!';


