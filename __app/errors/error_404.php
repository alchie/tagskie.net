<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="">
    <meta name="author" content="">
    
    <title>404 Error - Page Not Found</title>

    <!-- Bootstrap core CSS -->
    <link href="/assets/css/bootstrap.min.css" rel="stylesheet">

    <!-- Custom styles for this template -->
    <link href="/assets/css/styles.css" rel="stylesheet">

    <!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
      <script src="https://oss.maxcdn.com/libs/respond.js/1.3.0/respond.min.js"></script>
    <![endif]-->
    <style type="text/css">
    <!--
    #error {
       text-align:center; 
    }
     #error h1 {
        font-size:300px;
        text-transform:uppercase;
        color: #C7C7C7;
        text-shadow: 0px 1px 1px #AAA;
     }
      #error p {
        font-size:50px;
      }
      #wrap .container {
        padding-top: 120px;
      }
      #wrap {
        margin: 0 auto -110px;
        padding: 0 0 110px;
    }
    -->
    </style>
  </head>

  <body>

  <!-- Fixed navbar -->
  <div id="header">
    <div id="topnav" class="navbar navbar-inverse navbar-fixed-top" role="navigation">
      <div class="container">
        <div class="navbar-header">
          <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
            <span class="sr-only">Toggle navigation</span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
          </button>
          <a class="navbar-brand" href="/">Live in Davao City</a>
        </div>
        <div class="navbar-collapse collapse">
          <ul class="nav navbar-nav">
             <li><a href="/index.php/movies">Movies</a></li>
             <li><a href="/index.php/series">Series</a></li>
             <li><a href="/index.php/videos">Videos</a></li>
		   </ul>
		          </div><!--/.nav-collapse -->
      </div>
    </div>
  </div>
  <div id="wrap">   
  <div class="container" id="error">
    <h1>404</h1>
    <p>Page Not Found!</p>
  </div>
</div>
<div id="footer">
 <div class="container" id="bottom-links">
	
	<div class="row">
		<div class="col-xs-12 col-sm-8 col-md-8 left">
	<ul>
         <li><a href="/index.php/terms_of_service">Terms of Service</a></li>
              <li><a href="/index.php/privacy_policy">Privacy Policy</a></li>
	</ul>
			
		</div>
		<div class="col-xs-12 col-sm-4 col-md-4 right">
		     <p class="pull-right">Tagskie.Net by <a href="https://plus.google.com/+AlchieNetCafeDabaw" target="_blank">Alchie Net Cafe</a></p>
		</div>
	</div>
        

      </div>
    </div>


    <!-- Bootstrap core JavaScript
    ================================================== -->
    <!-- Placed at the end of the document so the pages load faster -->
    <script data-cfasync="false" type="text/javascript" src="/assets/js/jquery-1.10.2.min.js"></script>
    <script data-cfasync="false" type="text/javascript" src="/assets/js/bootstrap.min.js"></script>
     <script data-cfasync="false" type="text/javascript" src="/assets/js/indavao.custom.js"></script>
  </body>
</html>

