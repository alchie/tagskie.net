<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');
/*
| -------------------------------------------------------------------------
| URI ROUTING
| -------------------------------------------------------------------------
| This file lets you re-map URI requests to specific controller functions.
|
| Typically there is a one-to-one relationship between a URL string
| and its corresponding controller class/method. The segments in a
| URL normally follow this pattern:
|
|	example.com/class/method/id/
|
| In some instances, however, you may want to remap this relationship
| so that a different class/function is called than the one
| corresponding to the URL.
|
| Please see the user guide for complete details:
|
|	http://codeigniter.com/user_guide/general/routing.html
|
| -------------------------------------------------------------------------
| RESERVED ROUTES
| -------------------------------------------------------------------------
|
| There area two reserved routes:
|
|	$route['default_controller'] = 'welcome';
|
| This route indicates which controller class should be loaded if the
| URI contains no data. In the above example, the "welcome" class
| would be loaded.
|
|	$route['404_override'] = 'errors/page_missing';
|
| This route will tell the Router what URI segments to use if those provided
| in the URL cannot be matched to a valid route.
|
*/

$route['default_controller'] = "welcome";
$route['404_override'] = '';

$route['watch'] = "videos";
$route['watch/(:any)'] = "videos/watch/$1";

$route['tag'] = "videos";
$route['tag/(:any)'] = "videos/tag/$1";

$route['movie'] = "movies";
$route['movie/tag'] = "movies";
$route['movie/tag/(:any)'] = "movies/tag/$1";
$route['movie/genre'] = "movies";
$route['movie/genre/(:any)'] = "movies/genre/$1";
$route['movie/(:num)/(:any)'] = "movies/watch/$1/$2";

$route['tv-series'] = "series";
$route['tv-series/tag'] = "series";
$route['tv-series/tag/(:any)'] = "series/tag/$1";
$route['tv-series/genre'] = "series";
$route['tv-series/genre/(:any)'] = "series/genre/$1";
$route['tv-series/(:any)'] = "series/watch/$1";

$route['terms_of_service'] = "welcome/terms_of_service";
$route['privacy_policy'] = "welcome/privacy_policy";
$route['login'] = "welcome/login";

/* End of file routes.php */
/* Location: ./application/config/routes.php */
